<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day03</title>
	<style>
		@import url('https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400&display=swap');
	</style>
</head>
<body>

    <div class="container" style="height: 350px; width: 550px; margin: 0 auto; border: 1px solid #5b9bd5;display: flex; justify-content:center; flex-direction: column; align-items: center;">
		<form action="#" method="get">
			<div style="display: flex; width: 450px; justify-content: space-around; margin: 10px auto">
				<div style="width: 110px; background-color: #9cc2e5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
					<label style="font-family: 'Roboto', sans-serif;" ><?php echo "Họ và tên"?></label>
				</div>
				<input type="text" style="margin-left: 20px; height: 40px; width: 320px; border: 1px solid #42719b;">
			</div>
	
			<div style="display: flex; width: 450px; justify-content: left; margin: 10px auto">
				<div style="width: 109px; background-color: #9cc2e5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
					<label style="font-family: 'Roboto', sans-serif;" ><?php echo "Giới tính"?></label>
				</div>

				<div style="display:flex; align-items: center">
				<?php
				$gender = array("Nữ", "Nam");
				$counter = 1;
				for ($i = 0; $i < count($gender); $i++) {
					$temp = $i + $counter;
					echo "<input type='radio' id=$gender[$i] name='gender' style='margin-left:30px;' value=$temp>";
					echo "<label for=$gender[$i]>$gender[$i]</label>";
					$counter = -1;
				}
					?>
				</div>
			</div>
			
			<div  style="display: flex; width: 450px; margin: 10px auto">
				<div style="width: 110px; background-color: #9cc2e5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
					<label style="font-family: 'Roboto', sans-serif;" ><?php echo "Phân khoa"?></label>
				</div>		
				<select name="faculty" id="faculty" style="margin-left: 20px; height: 40px; width: 170px; border: 1px solid #42719b;">
					<?php 
					$faculties = array(array("None", ""), array("MAT", "Khoa học máy tính"), array("KDL", "Khoa học vật liệu"));
					foreach ($faculties as $faculty) {
						echo "<option value=$faculty[0]>$faculty[1]</option>";
					}
					?>
				</select>
			</div>
		</form>
		<div style="width: 300px; margin: 0 auto; display: flex; justify-content: center;">
			<input type="submit" value="Đăng ký" style="height: 45px; width: 130px; font-family: 'Roboto', sans-serif; font-size: 15px; background-color: #ed7d31; border-radius: 5px; margin-top: 30px; text-decoration: underline; font-weight: bold">
		</div>
    </div>
</body>
</html>